package com.signaltalk.nfcsampleinoplus

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.test.suitebuilder.TestSuiteBuilder
import android.util.Log
import android.widget.Button
import jp.innovationplus.nfc.USBFelicaListener
import kotlinx.android.synthetic.main.activity_main.*


@SuppressLint("SetTextI18n")
class MainActivity : AppCompatActivity(), USBFelicaListener {
    companion object {
        const val TAG = "MainActivity"
    }

    private val reader = JLCardReader()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        poll_a_button.setOnClickListener {
            it as Button
            if (reader.polling) {
                it.text = "POLL"
            } else {
                if (reader.startPoll()) {
                    it.text = "STOP"
                    main_text.text = "Polling..."
                } else {
                    main_text.text = "PaSoRi not opened."
                }
            }
        }
        reader.onErrorListner = {
            poll_a_button.text = "POLL"
            main_text.text = "Error: $it"
        }
        reader.onCardDetectListner = {
            poll_a_button.text = "POLL"
            main_text.text = "${it.type} - ${it.uid}"
        }

        if (!reader.onCreate(this)) {
            Log.e(TAG, "Failed to initialize UsbFelica")
            finish()
        }
    }

    override fun onDestroy() {
        reader.onDestroy(this)
        super.onDestroy()
    }

    override fun allowedPermission() {
        main_text.text = "PaSoRi OK."
    }

    override fun deniedPermission() {
        main_text.text = "PaSoRi denied."
    }

    override fun removed() {
        main_text.text = "PaSoRi を接続してください."
        poll_a_button.isEnabled = false
    }

    override fun inserted() {
        main_text.text = "PaSoRi が接続されています."
        poll_a_button.isEnabled = true
    }

}
