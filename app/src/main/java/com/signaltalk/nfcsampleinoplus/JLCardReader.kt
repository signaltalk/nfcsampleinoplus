package com.signaltalk.nfcsampleinoplus

import android.content.Context
import android.os.Handler
import android.util.Log
import jp.innovationplus.nfc.*
import java.util.*
import kotlin.experimental.and
import kotlin.experimental.or


class JLCardReader {
    companion object {
        val AID = hexToBytes("FF") + "JLCard2018".toByteArray()

        const val TAG = "JLCardReader"
        const val POLL_INTERVAL = 1000L
        const val POLL_INTERVAL_ON_SUICA = 500L

        val APDU_CMD_SELECT = hexToBytes("00A40400")
        val APDU_STATUS_OK = hexToBytes("9000")
        val APDU_STATUS_NOT_FOUND = hexToBytes("6A82")

        private fun hexDigit(h: Char) = Character.digit(h, 16)
        private fun hexToBytes(hex: String) = ByteArray(hex.length / 2) {
            ((hexDigit(hex[it * 2]) shl 4) + hexDigit(hex[it * 2 + 1])).toByte()
        }

        fun cardType(sak: Byte): String {
            return when (sak) {
                0x24.toByte() -> "MIFARE DESFire"
                0x00.toByte() -> "MIFARE Ultralight"
                0x09.toByte() -> "MIFARE Mini"
                0x08.toByte() -> "MIFARE Classic 1K"
                0x18.toByte() -> "MIFARE Classic 4K"
                0x10.toByte() -> "MIFARE Plus (2K, SL2)"
                0x11.toByte() -> "MIFARE Plus (4K, SL2)"
                0x20.toByte() -> "MIFARE DESFire / MIFARE Plus"
                else -> "SAK=${Util.toHex2String(sak.toInt())}"
            }
        }

        fun errorMessage(err: Int): String {
            return when (err) {
                IPNFCLibConstants.Error.SUCCESS -> "SUCCESS"
                IPNFCLibConstants.Error.INVALID_PARAM -> "INVALID_PARAM"
                IPNFCLibConstants.Error.TIMEOUT -> "TIMEOUT"
                IPNFCLibConstants.Error.DEVICE -> "DEVICE"
                IPNFCLibConstants.Error.FRAME_CRC -> "FRAME_CRC"
                IPNFCLibConstants.Error.INVALID_RESPONSE -> "INVALID_RESPONSE"
                IPNFCLibConstants.Error.BUF_OVERFLOW -> "BUF_OVERFLOW"
                else -> "Unknown"
            }
        }
    }

    data class JLCard(val type: String, val uid: String) {
        companion object {
            const val TYPE_SUICA = "Suica"
            const val TYPE_HCE = "HCE"
        }
    }

    var polling = false
    var onErrorListner: ((String) -> Unit)? = null
    var onCardDetectListner: ((JLCard) -> Unit)? = null

    private val usbFelica = USBFelica()
    private val handler = Handler()
    private var modeSuica = false

    fun onCreate(context: Context): Boolean {
        return usbFelica.onCreate(context)
    }

    fun onDestroy(context: Context) {
        usbFelica.onDestroy(context)
    }

    fun startPoll(): Boolean {
        if (!usbFelica.opened()) {
            Log.e(TAG, "PaSoRi not Opened.")
            return false
        }
        polling = true
        modeSuica = false
        handler.post(object : Runnable {
            override fun run() {
                if (polling) {
                    if (
                            if (modeSuica) {
                                Log.d(TAG, "one time, Suica polling.")
                                modeSuica = false
                                pollSuica() || pollSuica() || pollTypea()
                            } else {
                                pollTypea() || pollSuica() || pollSuica()
                            }
                    ) {
                        stopPoll()
                    } else {
                        if (modeSuica) {
                            handler.postDelayed(this, POLL_INTERVAL_ON_SUICA)
                        } else {
                            handler.postDelayed(this, POLL_INTERVAL)
                        }
                    }
                }
            }
        })
        return true
    }

    fun stopPoll() {
        usbFelica.rfOff()
        polling = false
    }

    private fun handleError(ret: Int) {
        val message = errorMessage(ret)
        Log.w(TAG, "Error: $message")
        onErrorListner?.let { it(message) }
    }

    private fun handleError(msg: String) {
        onErrorListner?.let { it(msg) }
    }

    private fun handleCard(type: String, uid: String) {
        Log.d(TAG, "Card detected: $type - $uid")
        onCardDetectListner?.let { it(JLCard(type, uid)) }
    }

    private fun pollTypea(): Boolean {
        Log.d(TAG, "typea_polling...")
        val ret = usbFelica.typea_polling(null, 0)
        Log.d(TAG, "typea_polling: $ret")
        if (ret == IPNFCLibConstants.Error.TIMEOUT) {
            return false
        }
        if (ret != IPNFCLibConstants.Error.SUCCESS) {
            handleError(ret)
            return false
        }
        val uid = Util.toHexString(Arrays.copyOf(usbFelica.uid, usbFelica.uid_len))
        Log.d(TAG, "uid=$uid, sak=${Util.toHex2String(usbFelica.sak.toInt())}")
        if (usbFelica.sak and 0x20.toByte() == 0x20.toByte()) {
            return if (connectISODEP()) {
                true
            } else {
                // RF切って、次回 Suica優先でポーリング
                usbFelica.rfOff()
                modeSuica = true
                false
            }
        }
        if (uid.startsWith("08")) {
            Log.w(TAG, "RID=$uid, it is random uid.")
            return false
        }
        handleCard(cardType(usbFelica.sak), uid)
        return true
    }

    private fun pollSuica(): Boolean {
        Log.d(TAG, "polling felica...")
        val ret = usbFelica.polling(0x0003) // Suica
        Log.d(TAG, "polling felica: ret=$ret")
        if (ret == IPNFCLibConstants.Error.TIMEOUT) {
            return false
        }
        if (ret != IPNFCLibConstants.Error.SUCCESS) {
            handleError(ret)
            return false
        }
        handleCard(JLCard.TYPE_SUICA, Util.toHexString(usbFelica.idm))
        return true
    }

    private fun connectISODEP(): Boolean {
        // ISO-DEP (ISO 14443-4) protocol
        val card = pollingType4A() ?: return false
        val payload = selectType4A(card, AID) ?: return false
        handleCard(JLCard.TYPE_HCE, String(payload))
        return true
    }

    private fun pollingType4A(): TypeaCard? {
        val param = Iso14443_4_Card.Iso14443_4_Param()
        param.useCid = true
        param.cid = 0x02.toByte()
        param.useNad = false
        param.fsdi = 0x08.toByte()
        param.drCapability = Iso14443_4_Card.ISO14443_4_CARD_D_CAPABILITY_212K or Iso14443_4_Card.ISO14443_4_CARD_D_CAPABILITY_424K
        param.dsCapability = Iso14443_4_Card.ISO14443_4_CARD_D_CAPABILITY_212K or Iso14443_4_Card.ISO14443_4_CARD_D_CAPABILITY_424K
        val info = Iso14443_4_Info()
        val ats = Iso14443_4_Card.Iso14443_4_Ats()
        val card = TypeaCard()
        var ret: Int = -1
        for (i in 1..2) {
            Log.d(TAG, "nfc100TypeaPolling...")
            ret = usbFelica.mAcc.nfc100TypeaPolling(null, 0, param, card, ats, info, 1000)
            Log.d(TAG, "nfc100TypeaPolling: ret = $ret")
            if (ret != IPNFCLibConstants.Error.TIMEOUT) break
        }
        if (ret != IPNFCLibConstants.Error.SUCCESS) {
            handleError(ret)
            return null
        }
        if (card.ats == null || card.ats.data == null) {
            Log.w(TAG, "The card ATS not present.")
            return null
        }
        Log.d(TAG, "ATS=${Util.toHexString(Arrays.copyOf(card.ats.data, card.ats.len[0]))}")
        if (card.sak and 0x20.toByte() != 0x20.toByte()) {
            Log.w(TAG, "SAK=${Util.toHex2String(card.sak.toInt())}: The card is not conform to ISO/IEC 14443-4")
            return null
        }
        return card
    }

    private fun selectType4A(card: TypeaCard, aid: ByteArray): ByteArray? {
        val command = APDU_CMD_SELECT + byteArrayOf(aid.size.toByte()) + aid
        val response = ByteArray(512)
        val responseSize = IntArray(1)
        Log.d(TAG, "command: ${Util.toHexString(command)}")
        val ret = usbFelica.mAcc.nfc100TypeaCommand(card, command, command.size, response.size, response, responseSize, null, 1000, 1000)
        if (ret != IPNFCLibConstants.Error.SUCCESS) {
            handleError(ret)
            return null
        }
        if (responseSize[0] < 2) {
            Log.w(TAG, "responseSize=${responseSize[0]}")
            return null
        }
        Log.d(TAG, "response: ${Util.toHexString(response.copyOf(responseSize[0]))}")
        val status = response.copyOfRange(responseSize[0] - 2, responseSize[0])
        when {
            status.contentEquals(APDU_STATUS_OK) -> return response.copyOf(responseSize[0] - 2)
            status.contentEquals(APDU_STATUS_NOT_FOUND) -> {
                handleError("Application(${Util.toHexString(aid)}) not found.")
            }
            else -> Log.w(TAG, "response: status = (${Util.toHexString(status)})")
        }
        return null
    }
}
